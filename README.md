# ETL Spark data flow #

- Programming languages: PYTHON, SCALA
- Big Data Tools: SPARK, GOOGLE CLOUD
- This project develops ETL engineering process for pollution data of Madrid.
- The program is developed in Python and Scala usign Spark in both languages.
- Tools needed for the processing have been installed and configured in a Google Cloud Instance 
   (Spark, Jupyter Notebook…).
- There are three main processes: batch processing for hourly and daily data streaming processing for 
   real time data.
-  The flow diagram represents the final target of the project.


![picture](images/images/arquitectura_v2.png)
