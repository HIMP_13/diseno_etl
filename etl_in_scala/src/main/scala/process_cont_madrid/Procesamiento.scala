package process_cont_madrid

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

//import org.apache.spark.SparkContext
//import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.expr
//import org.apache.spark.sql.SparkSession
//import org.apache.spark.SparkContext
//import org.apache.spark.sql.functions.{format_string, _}
//import org.apache.spark.sql.functions._
//import org.apache.spark.sql.types._
//import org.apache.spark.rdd.RDD
//import org.apache.spark.sql.Row

class Procesamiento {

  //Definition of argument variable "spark" that gets the initialization of Spark application

  //val spark: SparkSession = SparkSession.builder.enableHiveSupport.getOrCreate()

  val spark = Exec_SPK_session.spark


  //val spark: SparkSession = spark_arg

  //spark.sparkContext.setLogLevel("WARN")

  //val sparkContext: SparkContext = spark.sparkContext

  import spark.implicits._


  def process_datos_horarios_y_tiempoReal(file_name: String, rd_path_spark: String,
                                          wr_path_spk_grpH: String, wr_path_spk_grpV: String, wr_path_spk_join: String): Unit = {

    val load_ciudades = spark.read.format("csv").option("sep", ";").option("header", "true")
      .load(rd_path_spark + file_name)
    // Creamos una vista temporal en esta Spark session para poder ejecutar querys sobre el dataframe

    val temp1 = load_ciudades.createOrReplaceTempView("table")
    // Nos quedamos solo con la magnitud "8" que es el NO2

    val q1 = spark.sql("SELECT * FROM table WHERE magnitud = '8'")

    // Cargamos el resultado si es necesario

    // Para poder quedarnos solo con los válidos "V" y para facilitar el análisis posterior,
    // dado que tener cada hora en una columna me parece ineficiente, trasponemos las columnas
    // de Horas por filas y así tener una única columna para Horas y otra para Datos.

    val q2 = q1.select($"PROVINCIA", $"MUNICIPIO", $"ESTACION", $"MAGNITUD", $"PUNTO_MUESTREO",
      $"ANO", $"MES", $"DIA",
      expr(
        "stack(24, 'H01', H01, 'H02', H02, 'H03', H03, 'H04', H04, 'H05', H05, 'H06', H06, 'H07', "
          + "H07, 'H08', H08, 'H09', H09, 'H10', H10, 'H11', H11, 'H12', H12, 'H13', H13, 'H14', H14, 'H15', "
          + "H15, 'H16', H16, 'H17', H17, 'H18', H18, 'H19', H19, 'H20', H20, 'H21', H21, 'H22', H22, 'H23', "
          + "H23, 'H24', H24) as (HORAS,DATOS)"))


    // Cargamos el resultado en fichero. Dado que esta es una simulación no se carga
    // en un HDFS en una BD de Hive o en otra BD.

    q2.repartition(1).write.mode("overwrite").option("header", "true")
      .option("sep", ";").csv(wr_path_spk_grpH + file_name + LocalDateTime.now.format(DateTimeFormatter.ofPattern("_YYYYMMdd_HHmmss")))

    // Segunda parte del proceso anterior, creamos una única columna para Hora Validez, y Validez.

    val q3_pre = q1.select($"PROVINCIA", $"MUNICIPIO", $"ESTACION", $"MAGNITUD", $"PUNTO_MUESTREO",
      $"ANO", $"MES", $"DIA",
      expr(
        "stack(24, 'V01', V01, 'V02', V02, 'V03', V03, 'V04', V04, 'V05', V05, 'V06', V06, 'V07', "
          + "V07, 'V08', V08, 'V09', V09, 'V10', V10, 'V11', V11, 'V12', V12, 'V13', V13, 'V14', V14, 'V15', "
          + "V15, 'V16', V16, 'V17', V17, 'V18', V18, 'V19', V19, 'V20', V20, 'V21', V21, 'V22', V22, 'V23', "
          + "V23, 'V24', V24) as (HORA_VALIDEZ,VALIDEZ)"))

    // Caramos información.

    q3_pre.repartition(1).write.mode("overwrite").option("header", "true")
      .option("sep", ";").csv(wr_path_spk_grpV + file_name + LocalDateTime.now.format(DateTimeFormatter.ofPattern("_YYYYMMdd_HHmmss")))

    // Añadimos alias a el dataframe de columnas traspuestas ya que es necesario en el join posterior
    // para poder cargar la información sin obtener un error de duplicidad. Descartamos Hora_Validez

    val q3 = q3_pre.select($"PROVINCIA".alias("PROVINCIA_2"),
      $"MUNICIPIO".alias("MUNICIPIO_2"),
      $"ESTACION".alias("ESTACION_2"),
      $"MAGNITUD".alias("MAGNITUD_2"),
      $"PUNTO_MUESTREO".alias("PUNTO_MUESTREO_2"),
      $"ANO".alias("ANO_2"),
      $"MES".alias("MES_2"),
      $"DIA".alias("DIA_2"),
      $"VALIDEZ")

    // Ejecutamos un join entre los dataframes anteriores para obtener en un único dataframe
    // las columnas nuevas traspuestas Horas, Datos, Validez
    // Seleccionamos todos los campos del join resultante.

    val joinq2q3 = q2.join(q3, ($"PROVINCIA_2" === $"PROVINCIA") && (
      $"MUNICIPIO_2" === $"MUNICIPIO") &&
      ($"ESTACION_2" === $"ESTACION") && (
      $"MAGNITUD_2" === $"MAGNITUD") &&
      ($"PUNTO_MUESTREO_2" === $"PUNTO_MUESTREO") && (
      $"ANO_2" === $"ANO") &&
      ($"MES_2" === $"MES") && ($"DIA_2" === $"DIA"), "inner").select("*")



    // Cramos una vista temporal solo de los capos del join anterior que nos interesan.

    val joinq2q3_temp = joinq2q3.select("PROVINCIA", "MUNICIPIO", "ESTACION", "MAGNITUD",
      "PUNTO_MUESTREO",
      "ANO", "MES", "DIA", "HORAS", "DATOS",
      "VALIDEZ").createOrReplaceTempView("table_3")

    // Seleccionamos los campos necesarios, quedándonos solo con los válidos "V" y ordenando.

    val joinq2q3_post = spark.sql("SELECT PROVINCIA, MUNICIPIO, ESTACION, MAGNITUD, PUNTO_MUESTREO, "
      + "ANO, MES, DIA, HORAS, DATOS, VALIDEZ FROM table_3  WHERE VALIDEZ = 'V' "
      + "GROUP BY PROVINCIA, MUNICIPIO, ESTACION, MAGNITUD, PUNTO_MUESTREO, "
      + "ANO, MES, DIA, HORAS, DATOS, VALIDEZ "
      + "ORDER BY ESTACION, DIA, HORAS ASC")

    // Cargamos el resultado anterior que sería el final.

    joinq2q3_post.repartition(1).write.mode("overwrite").option("header", "true")
      .option("sep",";").csv(wr_path_spk_join + file_name + LocalDateTime.now.format(DateTimeFormatter.ofPattern("_YYYYMMdd_HHmmss")))

    joinq2q3_post.show(10)

    // A partir de aqui se podría continuar de la siguiente forma:
    // - Filtrar por los códigos de estación y por el día
    // - Se podría crear una Base de Datos en HDFS de HIVE o en cualquier otra BD
    // - Añadir nombres de estaciones y técnica usada
    // Ejemplo:
    // tabla_ciudades = spark.sql(
    //    "CREATE TABLE " + nombre_tabla + "(ciudad STRING, latitud STRING, longitud STRING) "
    //    + "ROW FORMAT DELIMITED "
    //    + "FIELDS TERMINATED BY ';' "
    //    + "LOCATION '/inputs/ciudades_coord' ")

    println("procesando...")

  }


  def process_datos_diarios(file_name: String): Unit = {

    val load_ciudades = spark.read.format("csv").option("sep", ";").option("header", "true").load(
      "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/runing_on_JupyterNotebook/datos_origen/datos_diarios/" + file_name)
    // Creamos una vista temporal en esta Spark session para poder ejecutar querys sobre el dataframe

    val temp1 = load_ciudades.createOrReplaceTempView("table")
    // Nos quedamos solo con la magnitud "8" que es el NO2

    val q1 = spark.sql("SELECT * FROM table WHERE magnitud = '8'")

    // Cargamos el resultado si es necesario

    // Para poder quedarnos solo con los válidos "V" y para facilitar el análisis posterior,
    // dado que tener cada hora en una columna me parece ineficiente, trasponemos las columnas
    // de Horas por filas y así tener una única columna para Horas y otra para Datos.

    val q2 = q1.select($"PROVINCIA", $"MUNICIPIO", $"ESTACION", $"MAGNITUD", $"PUNTO_MUESTREO",
      $"ANO", $"MES",
      expr(
        "stack(24, 'D01', D01, 'D02', D02, 'D03', D03, 'D04', D04, 'D05', D05, 'D06', D06, 'D07', "
          + "D07, 'D08', D08, 'D09', D09, 'D10', D10, 'D11', D11, 'D12', D12, 'D13', D13, 'D14', D14, 'D15', "
          + "D15, 'D16', D16, 'D17', D17, 'D18', D18, 'D19', D19, 'D20', D20, 'D21', D21, 'D22', D22, 'D23', "
          + "D23, 'D24', D24) as (DIAS,DATOS)"))


    // Cargamos el resultado en fichero. Dado que esta es una simulación no se carga
    // en un HDFS en una BD de Hive o en otra BD.

    q2.repartition(1).write.mode("overwrite").option("header", "true").option("sep", ";").csv(
      "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/etl_in_scala/salida/diarios/estaciones_grup_dias/"
        + file_name)

    // Segunda parte del proceso anterior, creamos una única columna para Hora Validez, y Validez.

    val q3_pre = q1.select($"PROVINCIA", $"MUNICIPIO", $"ESTACION", $"MAGNITUD", $"PUNTO_MUESTREO",
      $"ANO", $"MES",
      expr(
        "stack(24, 'V01', V01, 'V02', V02, 'V03', V03, 'V04', V04, 'V05', V05, 'V06', V06, 'V07', "
          + "V07, 'V08', V08, 'V09', V09, 'V10', V10, 'V11', V11, 'V12', V12, 'V13', V13, 'V14', V14, 'V15', "
          + "V15, 'V16', V16, 'V17', V17, 'V18', V18, 'V19', V19, 'V20', V20, 'V21', V21, 'V22', V22, 'V23', "
          + "V23, 'V24', V24) as (HORA_VALIDEZ,VALIDEZ)"))

    // Caramos información.

    q3_pre.repartition(1).write.mode("overwrite").option("header", "true").option("sep", ";").csv(
      "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/etl_in_scala/salida/diarios/estaciones_grup_val/"
        + file_name)

    // Añadimos alias a el dataframe de columnas traspuestas ya que es necesario en el join posterior
    // para poder cargar la información sin obtener un error de duplicidad. Descartamos Hora_Validez

    val q3 = q3_pre.select($"PROVINCIA".alias("PROVINCIA_2"),
      $"MUNICIPIO".alias("MUNICIPIO_2"),
      $"ESTACION".alias("ESTACION_2"),
      $"MAGNITUD".alias("MAGNITUD_2"),
      $"PUNTO_MUESTREO".alias("PUNTO_MUESTREO_2"),
      $"ANO".alias("ANO_2"),
      $"MES".alias("MES_2"),
      $"VALIDEZ")

    // Ejecutamos un join entre los dataframes anteriores para obtener en un único dataframe
    // las columnas nuevas traspuestas Horas, Datos, Validez
    // Seleccionamos todos los campos del join resultante.

    val joinq2q3 = q2.join(q3, ($"PROVINCIA_2" === $"PROVINCIA") && (
      $"MUNICIPIO_2" === $"MUNICIPIO") &&
      ($"ESTACION_2" === $"ESTACION") &&
      ($"MAGNITUD_2" === $"MAGNITUD") &&
      ($"PUNTO_MUESTREO_2" === $"PUNTO_MUESTREO") && (
      $"ANO_2" === $"ANO") &&
      ($"MES_2" === $"MES"), "inner").select("*")


    // Cramos una vista temporal solo de los capos del join anterior que nos interesan.

    val joinq2q3_temp = joinq2q3.select("PROVINCIA", "MUNICIPIO", "ESTACION", "MAGNITUD",
      "PUNTO_MUESTREO",
      "ANO", "MES", "DIAS", "DATOS",
      "VALIDEZ").createOrReplaceTempView("table_3")

    // Seleccionamos los campos necesarios, quedándonos solo con los válidos "V" y ordenando.

    val joinq2q3_post = spark.sql("SELECT PROVINCIA, MUNICIPIO, ESTACION, MAGNITUD, PUNTO_MUESTREO, "
      + "ANO, MES, DIAS, DATOS, VALIDEZ FROM table_3  WHERE VALIDEZ = 'V' "
      + "GROUP BY PROVINCIA, MUNICIPIO, ESTACION, MAGNITUD, PUNTO_MUESTREO, "
      + "ANO, MES, DIAS, DATOS, VALIDEZ "
      + "ORDER BY ESTACION, MES, DIAS ASC")

    // Cargamos el resultado anterior que sería el final.

    joinq2q3_post.repartition(1).write.mode("overwrite").option("header", "true").option("sep",";").csv(
      "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/etl_in_scala/salida/diarios/estaciones_join/"
        + file_name)

    joinq2q3_post.show(10)

    // A partir de aqui se podría continuar de la siguiente forma:
    // - Filtrar por los códigos de estación y por el día
    // - Se podría crear una Base de Datos en HDFS de HIVE o en cualquier otra BD
    // - Añadir nombres de estaciones y técnica usada
    // Ejemplo:
    // tabla_ciudades = spark.sql(
    //    "CREATE TABLE " + nombre_tabla + "(ciudad STRING, latitud STRING, longitud STRING) "
    //    + "ROW FORMAT DELIMITED "
    //    + "FIELDS TERMINATED BY ';' "
    //    + "LOCATION '/inputs/ciudades_coord' ")

    println("procesando...")

  }

}
