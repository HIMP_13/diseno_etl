package process_cont_madrid


import java.io._
import java.util.concurrent.TimeUnit

class Ingest_1 (arg_0: String, arg_1: String, arg_2: String) extends Procesamiento() {

  def controler(): Unit = {

    if (arg_0.contains("horarios")) datos_horarios_job()
    else println("datos horarios no procesados")
    if (arg_1.contains("diarios")) datos_diarios_job()
    else println("datos diarios no procesados")
    if (arg_2.contains("t_real")) datos_tiempoReal_job()
    else println("datos en tiempo real no procesados")

    }

  def datos_horarios_job(): Unit = {

    // Recorremos todos los archivos de datos horarios

    val og_path_fileSistem = "/home/isamed/PycharmProjects/etl_batch_streaming_v2/runing_on_JupyterNotebook/datos_origen/datos_horarios/"
    val rd_path_spark = "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/runing_on_JupyterNotebook/datos_origen/datos_horarios/"
    val wr_path_spk_grpH = "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/etl_in_scala/salida/horarios/estaciones_grup_horas/"
    val wr_path_spk_grpV = "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/etl_in_scala/salida/horarios/estaciones_grup_val/"
    val wr_path_spk_join = "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/etl_in_scala/salida/horarios/estaciones_join/"

    val list_of_files = new File(og_path_fileSistem).listFiles

    for (file_name <- list_of_files)
    // Dado que se descomprimen distintos tipos solo nos interesan los csv
      if (file_name.toString.contains(".csv")) process_datos_horarios_y_tiempoReal(file_name.toString.substring(106,118)
        , rd_path_spark, wr_path_spk_grpH, wr_path_spk_grpV, wr_path_spk_join)


    println("Finalizado OK procesamiento datos HORARIOS")

  }

  def datos_diarios_job(): Unit = {

    // Recorremos todos los archivos de datos diarios

    val path = "/home/isamed/PycharmProjects/etl_batch_streaming_v2/runing_on_JupyterNotebook/datos_origen/datos_diarios/"

    val list_of_files = new File(path).listFiles

    for (file_name <- list_of_files)
    // Dado que se descomprimen distintos tipos solo nos interesan los csv
      if (file_name.toString.contains(".CSV")) process_datos_diarios(file_name.toString.split("/").last)


    println("Finalizado OK procesamiento datos DIARIOS")


  }

  def datos_tiempoReal_job(): Unit = {

    // Recorremos todos los archivos de datos diarios

    //val path = "/home/isamed/PycharmProjects/etl_batch_streaming_v2_v2/runing_on_JupyterNotebook/datos_origen/tiempo_real/"

    val og_path_fileSistem = "/home/isamed/PycharmProjects/etl_batch_streaming_v2/runing_on_JupyterNotebook/datos_origen/tiempo_real/"
    val rd_path_spark = "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/runing_on_JupyterNotebook/datos_origen/tiempo_real/"
    val wr_path_spk_grpH = "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/etl_in_scala/salida/tiempo_real/estaciones_grup_horas/"
    val wr_path_spk_grpV = "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/etl_in_scala/salida/tiempo_real/estaciones_grup_val/"
    val wr_path_spk_join = "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/etl_in_scala/salida/tiempo_real/estaciones_join/"

    val list_of_files = new File(og_path_fileSistem).listFiles

    while (true) {

      for (file_name <- list_of_files)
      // Dado que se descomprimen distintos tipos solo nos interesan los csv
        if (file_name.toString.contains(".csv")) process_datos_horarios_y_tiempoReal(file_name.toString.split("/").last
          , rd_path_spark, wr_path_spk_grpH, wr_path_spk_grpV, wr_path_spk_join)


      println("Finalizado OK procesamiento datos en TIEMPO REAL")

      println("Siguiente checking dentro de 40 minutos...")

      TimeUnit.SECONDS.sleep(5)

    }


  }

}

