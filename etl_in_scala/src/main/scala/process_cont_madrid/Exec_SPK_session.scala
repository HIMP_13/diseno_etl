package process_cont_madrid

import org.apache.spark.sql.SparkSession

//Defining new object that extends DefinitionsMembers class for calling the SparkSession method

object Exec_SPK_session extends DefinitionsMembers {

    // overriding spark method from abstract class DefinitionsMembers so it can run the Spark Session when calling singleton object.

    override def spark: SparkSession = SparkSession.builder.enableHiveSupport.getOrCreate()

    def main(args: Array[String]): Unit = {

      // Calling spark method from object (instance)

      val newSP = spark

      newSP.sparkContext.setLogLevel("WARN")

      //Instance of class "Ingest_1" that initiates execution process calling the "controler()" method.
      val process2 = new Ingest_1(args(0), args(1), args(2))
      process2.controler()

  }

}



