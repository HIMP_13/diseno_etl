import sys
import os
from process_cont_madrid.master.classes_etl.etl_1 import Etl_1
from pyspark.sql import SparkSession

# Ejecutamos el proceso que necesitamos.
# INSTRUCCIONES DE EJECUCIÓN:
# Introducimos en job_3.process_1, el job que deseamos ejecutar. En este caso:
# - para el procesamiento de DATOS HORARIOS introducir "test1" en la posición 1
# - para el procesamiento de DATOS DIARIOS introducion "test2" en la posición 2
# - para el procesamiento de DATOS TIEMPO REAL introducion "test3" en la posición 2. Se ejecuta cada 40 minutos.

def main():
    #spark_init_1 = Etl_1()
    #spark_init_1.spark_build(SparkSession)
    #spark.sparkContext.setLogLevel("WARN")

    # Poner el path hacia $SPARK_HOME/bin
    os.chdir("/home/isamed/spark/bin")

    job_3 = Etl_1()
    job_3.spark_build(SparkSession)
    job_3.process_1("test1", "none", "none", "none")


if __name__ == "__main__":
    main()