import os
import time
import datetime

from process_cont_madrid.master.classes_etl.procesamiento import Procesamiento
from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import expr


# from pyspark import SparkFiles

# Definimos la clase Etl_1 que contendrá los métodos que procesarán la información.

# Heredamos la clase Procesamiento

class Etl_1(Procesamiento):

    # Definimos un método padre que será llamado desde el main y desde el que accederemos a los métodos
    # que procesarán la información propiamente.

    def process_1(self, n1: str, n2: str, n3: str, n4: str):

        # Añadimos un método para acceder únicamente a métodos deseados

        if n1.__contains__("test1"):

            self.datos_horarios_job()

        else:
            print("datos horarios job no ejecutado")

        if n2.__contains__("test2"):

            self.datos_diarios_job()

        else:
            print("datos diarios job no ejecutado")

        if n3.__contains__("test3"):

            self.datos_tiempoReal_job()

        else:
            print("datos tiempoReal job no ejecutado")

        if n4.__contains__("accounts_table"):

            # accounts_table()
            print("none)")

        else:
            print("")

        return None

    # Este método procesa la información HORARIA

    def datos_horarios_job(self):

        # Recorremos todos los archivos de datos horarios

        path = "/home/isamed/PycharmProjects/etl_batch_streaming_v2/runing_on_JupyterNotebook/datos_origen/datos_horarios/"

        list_of_files = os.listdir(path)

        # path que será usado por el método process_datos_horarios() para cargar los archivos según sean de datos horarios o tiempo real

        self.pathR = "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/runing_on_JupyterNotebook/datos_origen/datos_horarios/"

        # estos paths serán done se guardará la información procesada para datos HORARIOS

        self.pathW1 = "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/etl_in_python/salida/horarios/estaciones_grup_horas/"
        self.pathW2 = "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/etl_in_python/salida/horarios/estaciones_grup_val/"
        self.pathW3 = "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/etl_in_python/salida/horarios/estaciones_join/"

        for each_file in list_of_files:

            # Dado que se descomprimen distintos tipos solo nos interesan los csv

            if each_file.__contains__(".csv"):

                # Reemplazamos la variable que será el nombre de archivo de salida

                self.file_nameR = each_file

                self.file_nameW = self.file_nameR #+ str(datetime.datetime.now())

                self.process_datos_horarios_y_tiempo_real()

        print("OK datos diarios procesados")

        return None

    def datos_tiempoReal_job(self):

        path = "/home/isamed/PycharmProjects/etl_batch_streaming_v2/runing_on_JupyterNotebook/datos_origen/tiempo_real/"

        list_of_files = os.listdir(path)

        # path que será usado por el método process_datos_horarios() para cargar los archivos según sean de datos horarios o tiempo real

        self.pathR = "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/runing_on_JupyterNotebook/datos_origen/tiempo_real/"

        # estos paths serán done se guardará la información procesada para datos en TIEMPO REAL

        self.pathW1 = "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/etl_in_python/salida/tiempo_real/estaciones_grup_horas/"
        self.pathW2 = "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/etl_in_python/salida/tiempo_real/estaciones_grup_val/"
        self.pathW3 = "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/etl_in_python/salida/tiempo_real/estaciones_join/"

        while True:

            for each_file in list_of_files:

                # Dado que se descomprimen distintos tipos solo nos interesan los csv

                if each_file.__contains__(".csv"):
                    # Reemplazamos la variable que será el nombre de archivo de salida

                    self.file_nameR = each_file

                    self.file_nameW = self.file_nameR + str(datetime.datetime.now())

                    self.process_datos_horarios_y_tiempo_real()

            print("OK procesamiento datos en TIEMPO REAL\nSiguiente checking después de 40 minutos")

            time.sleep(2400)