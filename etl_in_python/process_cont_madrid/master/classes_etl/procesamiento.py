import os

from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import expr

class Procesamiento:

    # Iniciamos un SparkSession necesrio para entrar el contexto de Spark

    def spark_build(self, SparkSession_arg):

        self.spark = SparkSession_arg.builder.enableHiveSupport().getOrCreate()

        return None

    def process_datos_horarios_y_tiempo_real(self):

        # Leemos el fichero correspondiente creando un dataframe que contenga la información

        load_ciudades = self.spark.read.format("csv").option("sep", ";").option("header", "true").load(self.pathR + self.file_nameR)
        # Creamos una vista temporal en esta Spark session para poder ejecutar querys sobre el dataframe

        temp1 = load_ciudades.createOrReplaceTempView("table")
        # Nos quedamos solo con la magnitud "8" que es el NO2

        q1 = self.spark.sql("SELECT * FROM table WHERE magnitud = '8'")
        # Cargamos el resultado si es necesario

        # self.q1.repartition(1).write.mode('overwrite').option("header", "true").option("sep", ";").csv(
        # "file:///home/himedrano/jupyter_projects/contaminacion_madrid/salida/horarios/estaciones_bruto/"
        # + file_name)

        # Para poder quedarnos solo con los válidos "V" y para facilitar el análisis posterior,
        # dado que tener cada hora en una columna me parece ineficiente, trasponemos las columnas
        # de Horas por filas y así tener una única columna para Horas y otra para Datos.

        q2 = q1.select("PROVINCIA", "MUNICIPIO", "ESTACION", "MAGNITUD", "PUNTO_MUESTREO",
                                     "ANO", "MES", "DIA",
                                    expr(
                                        "stack(24, 'H01', H01, 'H02', H02, 'H03', H03, 'H04', H04, 'H05', H05, 'H06', H06, 'H07', "
                                        + "H07, 'H08', H08, 'H09', H09, 'H10', H10, 'H11', H11, 'H12', H12, 'H13', H13, 'H14', H14, 'H15', "
                                        + "H15, 'H16', H16, 'H17', H17, 'H18', H18, 'H19', H19, 'H20', H20, 'H21', H21, 'H22', H22, 'H23', "
                                        + "H23, 'H24', H24) as (HORAS,DATOS)"))

        # Cargamos el resultado en fichero. Dado que esta es una simulación no se carga
        # en un HDFS en una BD de Hive o en otra BD.

        q2.repartition(1).write.mode('overwrite').option("header", "true").option("sep", ";").csv(self.pathW1 + self.file_nameW)

        # Segunda parte del proceso anterior, creamos una única columna para Hora Validez, y Validez.

        q3_pre = q1.select("PROVINCIA", "MUNICIPIO", "ESTACION", "MAGNITUD", "PUNTO_MUESTREO",
                                    "ANO", "MES", "DIA",
                                    expr(
                                        "stack(24, 'V01', V01, 'V02', V02, 'V03', V03, 'V04', V04, 'V05', V05, 'V06', V06, 'V07', "
                                        + "V07, 'V08', V08, 'V09', V09, 'V10', V10, 'V11', V11, 'V12', V12, 'V13', V13, 'V14', V14, 'V15', "
                                        + "V15, 'V16', V16, 'V17', V17, 'V18', V18, 'V19', V19, 'V20', V20, 'V21', V21, 'V22', V22, 'V23', "
                                        + "V23, 'V24', V24) as (HORA_VALIDEZ,VALIDEZ)"))

        # Caramos información.

        q3_pre.repartition(1).write.mode('overwrite').option("header", "true").option("sep", ";").csv(self.pathW2 + self.file_nameW)

        # Añadimos alias a el dataframe de columnas traspuestas ya que es necesario en el join posterior
        # para poder cargar la información sin obtener un error de duplicidad. Descartamos Hora_Validez

        q3 = q3_pre.select((q3_pre.PROVINCIA).alias("PROVINCIA_2"),
                                     (q3_pre.MUNICIPIO).alias("MUNICIPIO_2"),
                                     (q3_pre.ESTACION).alias("ESTACION_2"),
                                     (q3_pre.MAGNITUD).alias("MAGNITUD_2"),
                                     (q3_pre.PUNTO_MUESTREO).alias("PUNTO_MUESTREO_2"),
                                     (q3_pre.ANO).alias("ANO_2"),
                                     (q3_pre.MES).alias("MES_2"),
                                     (q3_pre.DIA).alias("DIA_2"),
                                     "VALIDEZ")

        # Ejecutamos un join entre los dataframes anteriores para obtener en un único dataframe
        # las columnas nuevas traspuestas Horas, Datos, Validez
        # Seleccionamos todos los campos del join resultante.

        joinq2q3 = q2.join(q3, (q3.PROVINCIA_2 == q2.PROVINCIA) & (
                    q3.MUNICIPIO_2 == q2.MUNICIPIO) &
                                     (q3.ESTACION_2 == q2.ESTACION) & (
                                                 q3.MAGNITUD_2 == q2.MAGNITUD) &
                                     (q3.PUNTO_MUESTREO_2 == q2.PUNTO_MUESTREO) & (
                                                 q3.ANO_2 == q2.ANO) &
                                     (q3.MES_2 == q2.MES) & (q3.DIA_2 == q2.DIA),
                                     'inner').select(q2["*"], q3["*"])

        # Cramos una vista temporal solo de los capos del join anterior que nos interesan.

        joinq2q3_temp = joinq2q3.select("PROVINCIA", "MUNICIPIO", "ESTACION", "MAGNITUD",
                                                  "PUNTO_MUESTREO",
                                                  "ANO", "MES", "DIA", "HORAS", "DATOS",
                                                  "VALIDEZ").createOrReplaceTempView("table_3")
        # Seleccionamos los campos necesarios, quedándonos solo con los válidos "V" y ordenando.

        joinq2q3 = self.spark.sql("SELECT PROVINCIA, MUNICIPIO, ESTACION, MAGNITUD, PUNTO_MUESTREO, "
                                  + "ANO, MES, DIA, HORAS, DATOS, VALIDEZ FROM table_3  WHERE VALIDEZ = 'V' "
                                  + "GROUP BY PROVINCIA, MUNICIPIO, ESTACION, MAGNITUD, PUNTO_MUESTREO, "
                                  + "ANO, MES, DIA, HORAS, DATOS, VALIDEZ "
                                  + "ORDER BY ESTACION, DIA, HORAS ASC")

        # Cargamos el resultado anterior que sería el final.

        joinq2q3.repartition(1).write.mode('overwrite').option("header", "true").option("sep",";").csv(self.pathW3 + self.file_nameW)

        joinq2q3.show(10)

        # A partir de aqui se podría continuar de la siguiente forma:
        # - Filtrar por los códigos de estación y por el día
        # - Se podría crear una Base de Datos en HDFS de HIVE o en cualquier otra BD
        # - Añadir nombres de estaciones y técnica usada
        # Ejemplo:
        # self.tabla_ciudades = spark.sql(
        #    "CREATE TABLE " + self.nombre_tabla + "(ciudad STRING, latitud STRING, longitud STRING) "
        #    + "ROW FORMAT DELIMITED "
        #    + "FIELDS TERMINATED BY ';' "
        #    + "LOCATION '/inputs/ciudades_coord' ")


        return None

        # Este método procesa la información DIARIA

    def datos_diarios_job(self):

        # Recorremos todos los archivos de datos horarios

        path = "/home/isamed/PycharmProjects/etl_batch_streaming_v2/runing_on_JupyterNotebook/datos_origen/datos_diarios/"

        list_of_files = os.listdir(path)

        # list_of_files = map(lambda x:x.lower(), list_of_files_pre)

        for each_file in list_of_files:

            # Dado que se descomprimen distintos tipos solo nos interesan los csv

            if each_file.__contains__(".CSV"):

                # Reemplazamos la variable que será el nombre de archivo de salida

                file_name = each_file

                # Leemos el fichero correspondiente creando un dataframe que contenga la información

                load_ciudades = self.spark.read.format("csv").option("sep", ";").option("header", "true").load(
                    "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/runing_on_JupyterNotebook/datos_origen/datos_diarios/"
                    + file_name)
                # Creamos una vista temporal en esta spark session para poder ejecutar querys sobre el dataframe

                temp1 = load_ciudades.createOrReplaceTempView("table")
                # Nos quedamos solo con la magnitud "8" que es el NO2

                q1 = self.spark.sql("SELECT * FROM table WHERE magnitud = '8'")
                # Cargamos el resultado si es necesario

                # q1.repartition(1).write.mode('overwrite').option("header", "true").option("sep", ";").csv(
                # "file:///home/himedrano/jupyter_projects/contaminacion_madrid/salida/diarios/estaciones_bruto/"
                # + file_name)

                # Para poder quedarnos solo con los válidos "V" y para facilitar el análisis posterior,
                # dado que tener cada hora en una columna me parece ineficiente, trasponemos las columnas
                # de Horas por filas y así tener una única columna para Dias y otra para Datos.

                q2 = q1.select("PROVINCIA", "MUNICIPIO", "ESTACION", "MAGNITUD", "PUNTO_MUESTREO",
                                         "ANO", "MES",
                                         expr(
                                             "stack(24, 'D01', D01, 'D02', D02, 'D03', D03, 'D04', D04, 'D05', D05, 'D06', D06, 'D07', "
                                             + "D07, 'D08', D08, 'D09', D09, 'D10', D10, 'D11', D11, 'D12', D12, 'D13', D13, 'D14', D14, 'D15', "
                                             + "D15, 'D16', D16, 'D17', D17, 'D18', D18, 'D19', D19, 'D20', D20, 'D21', D21, 'D22', D22, 'D23', "
                                             + "D23, 'D24', D24) as (DIAS,DATOS)"))

                # Cargamos el resultado en fichero. Dado que esta es una simulación no se carga
                # en un HDFS en una BD de Hive o en otra BD.

                q2.repartition(1).write.mode('overwrite').option("header", "true").option("sep", ";").csv(
                    "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/etl_in_python/salida/diarios/estaciones_grup_dias/"
                    + file_name)

                # Segunda parte del proceso anterior, creamos una única columna para Hora Validez, y Validez.

                q3_pre = q1.select("PROVINCIA", "MUNICIPIO", "ESTACION", "MAGNITUD", "PUNTO_MUESTREO",
                                             "ANO", "MES",
                                             expr(
                                                 "stack(24, 'V01', V01, 'V02', V02, 'V03', V03, 'V04', V04, 'V05', V05, 'V06', V06, 'V07', "
                                                 + "V07, 'V08', V08, 'V09', V09, 'V10', V10, 'V11', V11, 'V12', V12, 'V13', V13, 'V14', V14, 'V15', "
                                                 + "V15, 'V16', V16, 'V17', V17, 'V18', V18, 'V19', V19, 'V20', V20, 'V21', V21, 'V22', V22, 'V23', "
                                                 + "V23, 'V24', V24) as (HORA_VALIDEZ,VALIDEZ)"))

                # Caramos información.

                q3_pre.repartition(1).write.mode('overwrite').option("header", "true").option("sep", ";").csv(
                    "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/etl_in_python/salida/diarios/estaciones_grup_val/"
                    + file_name)

                # Añadimos alias a el dataframe de columnas traspuestas ya que es necesario en el join posterior
                # para poder cargar la información sin obtener un error de duplicidad. Descartamos Hora_Validez

                q3 = q3_pre.select((q3_pre.PROVINCIA).alias("PROVINCIA_2"),
                                             (q3_pre.MUNICIPIO).alias("MUNICIPIO_2"),
                                             (q3_pre.ESTACION).alias("ESTACION_2"),
                                             (q3_pre.MAGNITUD).alias("MAGNITUD_2"),
                                             (q3_pre.PUNTO_MUESTREO).alias("PUNTO_MUESTREO_2"),
                                             (q3_pre.ANO).alias("ANO_2"),
                                             (q3_pre.MES).alias("MES_2"),
                                             "VALIDEZ")

                # Ejecutamos un join entre los dataframes anteriores para obtener en un único dataframe
                # las columnas nuevas traspuestas Horas, Datos, Validez
                # Seleccionamos todos los campos del join resultante.

                joinq2q3 = q2.join(q3, (q3.PROVINCIA_2 == q2.PROVINCIA) &
                                        (q3.MUNICIPIO_2 == q2.MUNICIPIO) &
                                        (q3.ESTACION_2 == q2.ESTACION) & (
                                                    q3.MAGNITUD_2 == q2.MAGNITUD) &
                                        (q3.PUNTO_MUESTREO_2 == q2.PUNTO_MUESTREO) & (
                                                    q3.ANO_2 == q2.ANO) &
                                        (q3.MES_2 == q2.MES), 'inner').select(q2["*"], q3["*"])

                # Cramos una vista temporal solo de los capos del join anterior que nos interesan.

                joinq2q3_temp = joinq2q3.select("PROVINCIA", "MUNICIPIO", "ESTACION", "MAGNITUD",
                                                          "PUNTO_MUESTREO",
                                                          "ANO", "MES", "DIAS", "DATOS",
                                                          "VALIDEZ").createOrReplaceTempView("table_3")
                # Seleccionamos los campos necesarios, quedándonos solo con los válidos "V" y ordenando.

                joinq2q3 = self.spark.sql("SELECT PROVINCIA, MUNICIPIO, ESTACION, MAGNITUD, PUNTO_MUESTREO, "
                                          + "ANO, MES, DIAS, DATOS, VALIDEZ FROM table_3  WHERE VALIDEZ = 'V' "
                                          + "GROUP BY PROVINCIA, MUNICIPIO, ESTACION, MAGNITUD, PUNTO_MUESTREO, "
                                          + "ANO, MES, DIAS, DATOS, VALIDEZ "
                                          + "ORDER BY ESTACION, MES, DIAS ASC")

                # Cargamos el resultado anterior que sería el final.

                joinq2q3.repartition(1).write.mode('overwrite').option("header", "true").option("sep",";").csv(
                    "file:///home/isamed/PycharmProjects/etl_batch_streaming_v2/etl_in_python/salida/diarios/estaciones_join/"
                    + file_name)

                joinq2q3.show(5)

                # A partir de aqui se podría continuar de la siguiente forma:
                # - Filtrar por los códigos de estación y por el día
                # - Se podría crear una Base de Datos en HDFS de HIVE o en cualquier otra BD
                # - Añadir nombres de estaciones y técnica usada
                # Ejemplo:
                # tabla_ciudades = self.spark.sql(
                #    "CREATE TABLE " + nombre_tabla + "(ciudad STRING, latitud STRING, longitud STRING) "
                #    + "ROW FORMAT DELIMITED "
                #    + "FIELDS TERMINATED BY ';' "
                #    + "LOCATION '/inputs/ciudades_coord' ")
            else:

                ""

        print("OK procesamiento de datos DIARIOS")

        return None